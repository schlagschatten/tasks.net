﻿using System;
using System.Text;

namespace Test
{
    public class Person
    {
        private string _name;
        private string _surname;
        private DateTime _dateBirth;
        
        public Person (string name, string surname, DateTime dateBirth)
        {
            Name = name;
            Surname = surname;
            DateBirth = dateBirth;
            DateBirth = dateBirth.ToLocalTime();
        }

        public Person (): this("Nastya", "Boichuk", new DateTime(2000, 2, 4))
        {
            
        }
        public string Name
        {
            get { return _name;}
            set { _name = value; }
        }

        public string Surname
        {
            get { return _surname;}
            set { _surname = value; }
        }

        public DateTime DateBirth
        {
            get { return _dateBirth;}
            set { _dateBirth = value; }
        }

        public int YearBirth
        {
            get {return _dateBirth.Year;} 
            set {_dateBirth = new DateTime(value, _dateBirth.Month, _dateBirth.Day);} 
        }
        public override string ToString()
        {
            var sb = new StringBuilder();
            
            sb.Append("name: ");
            sb.Append(Name);
            sb.Append( "\n");
            
            sb.Append("surname: ");
            sb.Append(Surname);
            sb.Append( "\n");
            
            sb.Append("date_birth: ");
            sb.Append(DateBirth.ToLongDateString());
            sb.Append( "\n");
            
            return sb.ToString(); 
        }

        public string ToShortString()
        {
            var sb = new StringBuilder();
            
            sb.Append("name: ");
            sb.Append(Name);
            sb.Append( "\n");
            
            sb.Append("surname: ");
            sb.Append(Surname);
            sb.Append( "\n");
            
            return sb.ToString(); 
        }
    }
}