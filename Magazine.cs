﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;

namespace Test
{
    public class Magazine
    {
        private string _nameArticle;
        private Frequency _periodicity;
        private DateTime _dateRelease;
        private int _circulation;
        private Article[] _list;

        public bool this[ Frequency index]
        {
            get
                {
                    if (index==_periodicity)
                    {
                        return true;
                    }
                    else
                    {
                      return false;   
                    }
                }
             
        }
      
        public Magazine(string nameArticle, Frequency periodicity, DateTime dateRelease, int circulation)
        {
            NameArticle = nameArticle;
            Periodicity = periodicity;
            DateRelease = dateRelease;
            Circulation = circulation;
            List = new Article[]{};
        }

        public Magazine() : this("StudLife", Frequency.Montly, DateTime.Now, 2)

        {
            
        }
        public string NameArticle
        {
            get {return _nameArticle;}
            set { _nameArticle = value; }
        }

        public Frequency Periodicity
        {
            get { return _periodicity; }
            set { _periodicity = value; }
        }

        public DateTime DateRelease
        {
            get { return _dateRelease; }
            set { _dateRelease = value; }
        }

        public int Circulation
        {
            get { return _circulation; }
            set { _circulation = value; }
        }

        public Article[] List
        {
            get { return _list; }
            set { _list = value; } 
        }
        
        //
        public double Seredn
        {
            get
            {
               
                double  sum = 0;
                for (int i = 0; i < _list.Length; i++)
                {
                    sum += _list[i]!=null? _list[i].RatingArticle:0;
                }
                return sum / _list.Length;
            }
        }

       public void AddArticles(params Article[] list)
        {
            Array.Resize(ref _list, _list.Length+list.Length);
            for (int i = 0; i < list.Length; i++)
            {
                _list[i+_list.Length-list.Length] = list[i];
            }
        }

       public override string ToString()
       {
           var sb = new StringBuilder();
           sb.Append("nameArticle: " );
           sb.Append(NameArticle);
           sb.Append( "\n");
           
           sb.Append("new Frequency(): ");
           sb.Append(_periodicity);
           sb.Append( "\n");
           
           sb.Append("new DateTime()");
           sb.Append(_dateRelease);
           sb.Append( "\n");
           
           sb.Append("circulation: ");
           sb.Append(_circulation);
           sb.Append( "\n");

           sb.Append("Articles:");
           foreach(Article a in List)
           {
               sb.Append(a);
               sb.Append("\n");
           }

           return sb.ToString();
       }
       

       public  string ToShortString()
       {
           var sb = new StringBuilder();
           sb.Append("nameArticle: " );
           sb.Append(NameArticle);
           sb.Append( "\n");
           
           sb.Append("new Frequency(): ");
           sb.Append(_periodicity);
           sb.Append( "\n");
           
           sb.Append("new DateTime()");
           sb.Append(_dateRelease);
           sb.Append( "\n");
           
           sb.Append("circulation: ");
           sb.Append(_circulation);
           sb.Append( "\n");
           
           sb.Append("new Article[]{}");
           sb.Append(Seredn);
           sb.Append( "\n");
           
           return sb.ToString();
       }
    }
}